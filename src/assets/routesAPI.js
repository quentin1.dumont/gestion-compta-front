const BASE_URL = 'https://oracle.anthony-jeanney.fr/';
const COMPTA = BASE_URL + 'gestionCompta/api/'
const RH = BASE_URL + 'gestionRH/api/api/'

const ROUTES_API = {
    FACTURES: COMPTA + 'factures/',
    FICHES_DE_PAIE: COMPTA + 'fichesDePaie/',
    PERSONNEL: RH + 'personnel/',
}

export default ROUTES_API;
import { createRouter, createWebHistory } from 'vue-router'

import Accueil from '@/views/Accueil.vue'

import FicheDePaieId from '@/views/FicheDePaieId.vue'
import FichesDePaies from '@/views/FichesDePaies.vue'
import CreerFicheDePaie from '@/views/CreerFicheDePaie.vue'
import ModifierFicheDePaie from '@/views/ModifierFicheDePaie.vue'

import FactureId from '@/views/FactureId.vue'
import Factures from '@/views/Factures.vue'
import CreerFacture from '@/views/CreerFacture.vue'
import ModifierFacture from '@/views/ModifierFacture.vue'

const router = createRouter({
    history: createWebHistory(
        import.meta.env.BASE_URL),
    routes: [{
            path: '/',
            name: 'Accueil',
            component: Accueil
        },
        {
            path: '/fiches_de_paie',
            name: 'Fiches de Paies',
            component: FichesDePaies
        },
        {
            path: '/fiches_de_paie/:id',
            name: 'Fiche de paie',
            component: FicheDePaieId,
            props: true
        },
        {
            path: '/fiches_de_paie/creer',
            name: 'Créer une fiche de paie',
            component: CreerFicheDePaie
        },
        {
            path: '/fiches_de_paie/:id/modifier',
            name: 'Modifier une fiche de paie',
            component: ModifierFicheDePaie,
            props: true
        },
        {
            path: '/factures',
            name: 'Factures',
            component: Factures
        },
        {
            path: '/factures/:id',
            name: 'Facture',
            component: FactureId,
            props: true
        },
        {
            path: '/factures/creer',
            name: 'Créer une facture',
            component: CreerFacture
        },
        {
            path: '/factures/:id/modifier',
            name: 'Modifier une facture',
            component: ModifierFacture,
            props: true
        },
        {
            path: '/:pathMatch(.*)*',
            redirect: '/'
        }
    ]
})

export default router